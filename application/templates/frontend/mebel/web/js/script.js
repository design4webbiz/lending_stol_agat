/**
 * Created by MI on 04.02.2017.
 */

$(function(){
    var ink, d, x, y;
    $(".ripplelink").click(function(e){
        if($(this).find(".ink").length === 0){
            $(this).prepend("<span class='ink'></span>");
        }

        ink = $(this).find(".ink");
        ink.removeClass("animate");

        if(!ink.height() && !ink.width()){
            d = Math.max($(this).outerWidth(), $(this).outerHeight());
            ink.css({height: d, width: d});
        }

        x = e.pageX - $(this).offset().left - ink.width()/2;
        y = e.pageY - $(this).offset().top - ink.height()/2;

        ink.css({top: y+'px', left: x+'px'}).addClass("animate");
    });
});
(function($) {
    $.fn.spinner = function() {
        this.each(function() {
            var el = $(this);

            // add elements
            el.wrap('<span class="spinner"></span>');
            el.before('<span class="sub">&or;</span>');
            el.after('<span class="add">&and;</span>');

            // substract
            el.parent().on('click', '.sub', function () {
                console.log('min');
                if (el.val() > parseInt(el.attr('min')))
                    el.val( function(i, oldval) { return --oldval; });
            });

            // increment
            el.parent().on('click', '.add', function () {
                console.log('max');
                if (el.val() < parseInt(el.attr('max'))) {
                    el.val( function(i, oldval) { return ++oldval; });
                }
            });
        });
    };

    $(document).ready(function () {
        $('input[type="number"]').spinner();
    });
})(jQuery);

(function($) {

    $(document).ready(function () {

        $(document).on('addCartElement', function (event, data) {
            $("#buyModal").modal('show');
            $("#buyModal").find('#formbuilder-product').val(data.CartElement.name);
            $("#buyModal").find('#formbuilder-count').val(data.CartElement.count);
            var prd = oakcms.modificationconstruct.getData(data.CartElement.options);
            if(prd) {
                $("#buyModal").find('#formbuilder-type').val(prd.name);
            }
        });

        $(document).on('shopSetModification', function (event, m) {
            $('.product__gallery [data-uk-slideshow-item="'+m.index+'"] a').trigger('click');
        });

        $('.scrollbar-light').scrollbar();

        $('header a[href^="#"], .carousel-caption a[href^="#"], .category a[href^="#"]').on('click', function () {
            $('html, body').stop().animate({
                scrollTop: $($(this).attr('href')).offset().top - 100
            }, 1000);
        });

        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('hide');

        $(".fb_form").on('beforeSubmit', function () {
            var form = $(this);
            var error = false;

            if (!error) {
                var data = form.serialize();
                $.ajax({
                    type: 'POST',
                    url: form.attr('action') + '?format=json',
                    dataType: 'json',
                    data: data,
                    beforeSend: function (data) {
                        form.find('button[type="submit"]').attr('disabled', 'disabled');
                    },
                    success: function (data) {

                        if (data['result'] == 'error') {
                            sweetAlert("Oops...", data['error'], "error");
                        } else {
                            form.trigger('reset');
                            form.closest('.modal').modal('hide');
                            sweetAlert({
                                title: '',
                                text: data['success'],
                                type: "success",
                                html: true
                            });
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    },
                    complete: function (data) {
                        form.find('button[type="submit"]').prop('disabled', false);
                    }
                });
            }
            return false;
        });
    });
})(jQuery);


