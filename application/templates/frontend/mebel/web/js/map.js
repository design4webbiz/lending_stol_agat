var map;
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: { lat: 50.5216626, lng: 30.6163604 },
        zoomControl: true,
        scrollwheel: false,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP]
        }
    });

    var iconBase = 'img/';

    var church = new google.maps.Marker({
        position: {lat: 50.5216626, lng: 30.6150729},
        map: map,
        icon: iconBase + 'logo-map.png'
    });

    var infowindow = new google.maps.InfoWindow({
        content: 'PROSTOLED'
    });
    infowindow.open(map, church);
    church.addListener('click', function() {
        infowindow.open(map, church);
    });
}