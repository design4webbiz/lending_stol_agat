/**
 * Created by MI on 13.03.2017.
 */
ymaps.ready(function () {
    var $map = $('.map'),
        myMap = new ymaps.Map('map', {
            center: [getLng('coordinate-center-lat'), getLng('coordinate-center-lng')],
            zoom: getLng('coordinate-zoom')
        }, {
            searchControlProvider: 'yandex#search'
        }),
        myPlacemark = new ymaps.Placemark([getLng('coordinate-lat'), getLng('coordinate-lng')], {
            hintContent: $map.data('coordinate-content')
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: $map.data('coordinate-icon'),
            // Размеры метки.
            iconImageSize: [30, 42],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-5, -38]
        }),
        myPlacemark2 = new ymaps.Placemark([getLng('coordinate-lat2'), getLng('coordinate-lng2')], {
            hintContent: $map.data('coordinate-content2')
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: $map.data('coordinate-icon2'),
            // Размеры метки.
            iconImageSize: [30, 42],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-5, -38]
        });

    myMap.geoObjects.add(myPlacemark);
    myMap.geoObjects.add(myPlacemark2);

    myMap.behaviors
    // Отключаем часть включенных по умолчанию поведений:
    //  - drag - перемещение карты при нажатой левой кнопки мыши;
    //  - magnifier.rightButton - увеличение области, выделенной правой кнопкой мыши.
        .disable(['drag', 'rightMouseButtonMagnifier', 'scrollZoom']);

});

function getLng(data) {
    var $map = $('.map');
    return parseFloat($map.data(data));
}
