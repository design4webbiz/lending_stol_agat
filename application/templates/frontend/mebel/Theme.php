<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 12.12.2016
 * Project: oakcms
 * File name: FrontendTheme.php
 */

namespace app\templates\frontend\mebel;

use Yii;

/**
 * Class Theme
 */
class Theme extends \yii\base\Theme
{

    public $themeSourcePath;

    public static $menuItemLayouts = [
        '@frontendTemplate/views/layouts/_clear'  => 'clear',
        '@frontendTemplate/views/layouts/content' => 'content',
        '@frontendTemplate/views/layouts/main'    => 'main'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->themeSourcePath = '@app/templates/frontend/mebel/web';

        $this->basePath = '@app/templates/frontend/mebel';
        $this->baseUrl = '@web/templates/frontend/mebel/web';

        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
            'sourcePath' => '@app/templates/frontend/mebel/web',
            'css' => ['css/bootstrap.css']
        ];

        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = [
            'sourcePath' => '@app/templates/frontend/mebel/web',
            'js' => ['//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js']
        ];

        Yii::$app->assetManager->bundles['yii\jui\JuiAsset'] = [
            'css' => ['//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.min.css'],
            'js'  => ['//code.jquery.com/ui/1.12.0/jquery-ui.min.js'],
        ];

        Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
            'js'  => ['//code.jquery.com/jquery-2.2.4.min.js'],
        ];

        $this->pathMap = [
            '@app/views'   => '@app/templates/frontend/mebel/views',
            '@app/modules' => '@app/templates/frontend/mebel/modules',
            '@app/widgets' => '@app/templates/frontend/mebel/widgets',
        ];
    }
}
