<?php


namespace app\templates\frontend\mebel\widgets\geoip;


use yii\bootstrap\Widget;
use app\templates\frontend\mebel\widgets\geoip\GetGeoIP;
use yii\helpers\VarDumper;

class GeoIP extends Widget
{
    protected $_ip;

    public function init()
    {
        $ip = \Yii::$app->request->userIP;
        if(\Yii::$app->request->userIP == '127.0.0.1' || \Yii::$app->request->userIP == '::1') {
            $ip = '194.126.181.189';
        }
        $this->_ip = $ip;
    }

    public function run()
    {
        $SxGeo = new GetGeoIP(dirname(__FILE__).'/SxGeoCity.dat');
        $dataCity = $SxGeo->getCityFull($this->_ip);
        //VarDumper::dump($dataCity);

        //echo $dataCity['city']['name_ru'];
    }
}
