<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'order_form',
    'title' => Yii::t('text', 'Order Form'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/order_form/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/order_form/view.php',
    'settings' => [
        'class' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'headerForm' => [
            'type' => 'textInput',
            'value' => 'ЗАКАЗАТЬ СТОЛ НА ДОМ!'
        ],
        'form' => [
            'type' => 'formBuilder',
            'value' => ''
        ]
    ],
];
