<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use yii\helpers\Html;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<section class="orderForm <?= $model->getSetting('class') ?>">
    <div class="container">
        <div class="col-md-4 orderForm__ttl">
            <span>
                <?= $model->getSetting('headerForm') ?>
            </span>
        </div>
        <div class="col-md-8 orderForm__form">
            [form_builder id="<?= $model->getSetting('form') ?>"]
        </div>
    </div>
</section>
