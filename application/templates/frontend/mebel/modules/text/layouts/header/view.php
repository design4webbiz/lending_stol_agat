<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\system\components\Menu;
use app\modules\menu\api\Menu as ApiMenu;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>
<header class="navbar-fixed-top">
    <section class="bran-nav-quest_form">
        <div class="container">
            <nav class="navbar navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" <?= $isHome ? '' : 'href="'.\yii\helpers\Url::home().'"' ?>>
                        <?= $model->settings['logo']['value'] ?>
                        <span>
                            <?= $model->settings['small']['value'] ?>
                        </span>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php
                    $items = ApiMenu::getMenuLvl($model->getSetting('menuHome'), 1, 1);
                    echo Menu::widget([
                        'items' => $items,
                        'options' => [
                            'class' => 'nav navbar-nav',
                            'id' => 'navlist'
                        ]
                    ]);
                    ?>
                </div><!-- /.navbar-collapse -->
            </nav>
            <div class="header_rigth-blck">
                <div class="select-city">
                    <span class="select-city_ttl">
                        Ваш город
                    </span>
                    <?= \app\templates\frontend\mebel\widgets\geoip\GeoIP::widget(); ?>
                    <? //var_dump(\Symfony\Component\Yaml\Yaml::parse($model->getSetting('phoneRegions'))) ?>
                    <select class="">
                        <option>москва</option>
                        <option>СПБ</option>
                        <option>уфа</option>
                        <option>калининград</option>
                        <option>тула</option>
                    </select>
                </div>
                <button class="quest-top ripplelink" data-toggle="modal" data-target="#<?= $model->getSetting('modalForm') ?>"><?= $model->getSetting('buttonName') ?></button>
            </div>
        </div>
    </section>
</header>
