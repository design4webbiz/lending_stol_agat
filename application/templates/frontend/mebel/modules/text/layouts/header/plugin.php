<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'header',
    'title' => Yii::t('text', 'Header'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/header/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/header/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'logo' => [
            'type' => 'textInput',
            'value' => 'СТОЛ АГАТ'
        ],
        'small' => [
            'type' => 'textInput',
            'value' => '18.10'
        ],
        'menuHome' => [
            'type' => 'menuType',
            'value' => '5'
        ],
        'phoneRegions' => [
            'type' => 'aceEditor',
            'value' => '',
            'mode' => 'yaml',
            'hint' => 'Phones for Regions, demo: <br> Москва: "+7000000000"<br> Питер: "+7000000000"'
        ],
        'buttonName' => [
            'type' => 'textInput',
            'value' => 'ЗАДАТЬ ВОПРОС'
        ],
        'modalForm' => [
            'type' => 'textInput',
            'value' => ''
        ]
    ],
];
