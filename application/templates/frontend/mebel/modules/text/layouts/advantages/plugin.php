<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'advantages',
    'title' => Yii::t('text', 'Advantages'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/advantages/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/advantages/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'advTitle' => [
            'type' => 'textInput',
            'value' => 'Преймущества стола Агат 28.10'
        ],
        'advImg' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'advBg' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'advSecurTitle' => [
            'type' => 'textInput',
            'value' => 'безопесность'
        ],
        'advSecurIco' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'advSecurDescription' => [
            'type' => 'textInput',
            'value' => 'Произведен из ЛДСП высокого качества наивысшего класса экологической безопасности E1.'
        ],
        'advStrongTitle' => [
            'type' => 'textInput',
            'value' => 'прочность'
        ],
        'advStrongIco' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'advStrongDescription' => [
            'type' => 'textInput',
            'value' => 'Благодаря ЛДСП высокой плотности 680 кг/м3 выдерживает высокие механические нагрузки.'
        ],
        'advResistanceTitle' => [
            'type' => 'textInput',
            'value' => 'ИЗНОСОСТОЙКОСТЬ'
        ],
        'advResistanceIco' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'advResistanceDescription' => [
            'type' => 'textInput',
            'value' => 'Поверхность стола не коробится от высокой температуры (пролитого горячего чая, забытого разогретого паяльника). Противоударный кант ПВХ Надёжно защищает края изделий от сколов и влаги, увеличивает срок службы мебели.'
        ],
        'advDesigngTitle' => [
            'type' => 'textInput',
            'value' => 'ДИЗАЙН и функциональность'
        ],
        'advDesigngIco' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'advDesignDescription' => [
            'type' => 'textInput',
            'value' => 'Глиттерный рисунок Устойчив к истиранию. Добавляет блеск интерьеру. Дополнительная полка  для хранения журналов и мелочей добавляет изделию функциональность.'
        ],
        'advVarantyTitle' => [
            'type' => 'textInput',
            'value' => '8 лет гарантии'
        ],
        'advVarantyIco' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'advVarantyDescription' => [
            'type' => 'textInput',
            'value' => 'Официальная гарантия от производителя 8 лет.'
        ]
    ],
];
