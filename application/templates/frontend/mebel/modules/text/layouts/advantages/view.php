<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<section class="advantages" style="background-image: url('<?= $model->settings['advBg']['value'] ?>')">
    <div class="container-fluid advantages__second-bg">
        <img src="images/advantages-img-bg.png">
    </div>
    <h3 class="h3-ttl advantages__ttl"><?= $model->settings['advTitle']['value'] ?></h3>
    <div class="container">
        <div class="col-sm-6 advantages__img">
            <img src="<?= $model->settings['advImg']['value'] ?>">
        </div>
        <div class="col-sm-6">
            <div class="advantages__secur advantages_item">
                <div class="advantages__small-ttl">
                    <span><?= $model->settings['advSecurTitle']['value'] ?></span>
                    <img src="<?= $model->settings['advSecurIco']['value'] ?>" alt="">
                </div>
                <div class="advantages__desc">
                    <span><?= $model->settings['advSecurDescription']['value'] ?></span>
                </div>
            </div>
            <div class="advantages__strong advantages_item">
                <div class="advantages__small-ttl">
                    <span><?= $model->settings['advStrongTitle']['value'] ?></span>
                    <img src="<?= $model->settings['advStrongIco']['value'] ?>" alt="">
                </div>
                <div class="advantages__desc">
                    <span><?= $model->settings['advStrongDescription']['value'] ?></span>
                </div>
            </div>
            <div class="advantages__resistance advantages_item">
                <div class="advantages__small-ttl">
                    <span><?= $model->settings['advResistanceTitle']['value'] ?></span>
                    <img src="<?= $model->settings['advResistanceIco']['value'] ?>" alt="">
                </div>
                <div class="advantages__desc">
                    <span><?= $model->settings['advResistanceDescription']['value'] ?></span>
                </div>
            </div>
            <div class="advantages__design advantages_item">
                <div class="advantages__small-ttl">
                    <span><?= $model->settings['advDesigngTitle']['value'] ?></span>
                    <img src="<?= $model->settings['advDesigngIco']['value'] ?>" alt="">
                </div>
                <div class="advantages__desc">
                    <span><?= $model->settings['advDesignDescription']['value'] ?></span>
                </div>
            </div>
            <div class="advantages__varanty  advantages_item">
                <div class="advantages__small-ttl">
                    <span><?= $model->settings['advVarantyTitle']['value'] ?></span>
                    <img src="<?= $model->settings['advVarantyIco']['value'] ?>" alt="">
                </div>
                <div class="advantages__desc">
                    <span><?= $model->settings['advVarantyDescription']['value'] ?></span>
                </div>
            </div>
        </div>
    </div>
</section>
