<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>
<section class="ship-pay">
    <div class="container clearfix">
        <div class="col-sm-6">
            <div class="ship-pay__cntnt">
                <h2 id="ship-pay" class="ttl"><?= $model->getSetting('shipingTitle') ?></h2>
                <div class="ship-pay__cntnt_img">
                    <img src="<?= $model->getSetting('shipingImage') ?>" alt="">
                </div>
                <div class="ship-pay__cntnt_desc">
                    <span>
                        <?= $model->getSetting('shipingDescription') ?>
                    </span>
                </div>
            </div>
            <div class="ship-pay__cntnt_back">
                <div class="ship-pay_bg"></div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="ship-pay__cntnt">
                <h2 class="ttl"><?= $model->getSetting('paymentTitle') ?></h2>
                <div class="ship-pay__cntnt_img">
                    <img src="<?= $model->getSetting('paymentImage') ?>" alt="">
                </div>
                <div class="ship-pay__cntnt_desc">
                    <span>
                        <?= $model->getSetting('paymentDescription') ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>
