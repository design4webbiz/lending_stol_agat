<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'shiping_payment',
    'title' => Yii::t('text', 'Shiping Payment'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/shiping_payment/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/shiping_payment/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'shipingTitle' => [
            'type' => 'textInput',
            'value' => 'ДОСТАВКА'
        ],
        'shipingImage' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'shipingDescription' => [
            'type' => 'textarea',
            'value' => '1. Доставка по всей территории РОССИИ.<br>Стоимость доставки: оплата заказчиком. Сотрудничаем с ведущими почтовыми 
службами: Автолюкс, Гюнсел, Ночной Экспресс, МОст Експрес.<br><br>r2. Самовывоз - бесплатно.'
        ],
        'paymentTitle' => [
            'type' => 'textInput',
            'value' => 'ОПЛАТА'
        ],
        'paymentImage' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'paymentDescription' => [
            'type' => 'textarea',
            'value' => 'У нас нет никаких скрытых платежей или накруток. Цена и условия покупки
полностью соответствуют условиям указанным на сайте.'
        ],
    ],
];
