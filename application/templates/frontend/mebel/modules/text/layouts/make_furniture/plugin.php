<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'make_furniture',
    'title' => Yii::t('text', 'Make Furniture'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/make_furniture/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/make_furniture/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'bg' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'title' => [
            'type' => 'textInput',
            'value' => 'так мы делаем нашу мебель'
        ],
        'desc' => [
            'type' => 'textInput',
            'value' => 'Посмотрите фотографии высокотехнологичного производства и убедитесь в надежности представленного стола.'
        ],
        'widget' => [
            'type' => 'widgetkit',
            'value' => ''
        ],
        'video-desc' => [
            'type' => 'textInput',
            'value' => 'Посмотрите видео о производстве на мебельной фабрике, где был изготовлен стол Агат 28.10'
        ],
        'video' => [
            'type' => 'textInput',
            'value' => ''
        ]
    ],
];
