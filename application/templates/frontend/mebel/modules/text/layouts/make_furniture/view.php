<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<section class="make-furniture" style="background-image: url('<?= $model->getSetting('bg') ?>')">
    <div class="container">
        <h2 id="make-furniture_ttl" class="ttl"><?= $model->getSetting('title') ?></h2>
        <div class="make-furniture_desc">
            <span>
                <?= $model->getSetting('desc') ?>
            </span>
        </div>
        <div class="make-furniture_widget scrollbar-light">
            [widgetkit id='<?= $model->getSetting('widget') ?>']
        </div>
        <div class="make-furniture__video-blc">
            <span>
                <?= $model->getSetting('video-desc') ?>
            </span>
            <div class="make-furniture__video-blc_video">
                <?= $model->getSetting('video') ?>
            </div>
        </div>
    </div>
</section>
