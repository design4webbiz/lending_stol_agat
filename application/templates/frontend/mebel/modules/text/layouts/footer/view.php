<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>
<section class="footer" style="background-color: <?= $model->getSetting('background') ?>">
    <div class="container">
        <div class="col-sm-4 footer_name">
            <span>
                <?= $model->getSetting('name') ?>
            </span>
        </div>
        <div class="col-sm-4">
            <img src="application/templates/frontend/mebel/web/img/phone.png">
            <span>
                <?= $model->getSetting('phone') ?>
            </span>
        </div>
        <div class="col-sm-4">
            <span>
               &copy;&nbsp;<?= $model->getSetting('copyright') ?>
            </span>
        </div>
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="" data-toggle="tooltip" data-placement="left" data-original-title="ВВЕРХ" style="display: inline; z-index: 9999"><img src="images/scroll-to-top.png" alt=""></a>
</section>
