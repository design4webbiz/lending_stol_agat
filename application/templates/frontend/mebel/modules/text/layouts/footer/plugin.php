<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'footer',
    'title' => Yii::t('text', 'Footer'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/footer/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/footer/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'name' => [
            'type' => 'textInput',
            'value' => 'Стол Агат 28.10'
        ],
        'background' => [
            'type' => 'textInput',
            'value' => '#cf1a41'
        ],
        'phone' => [
            'type' => 'textInput',
            'value' => '+0 800  880-22-50'
        ],
        'copyright' => [
            'type' => 'textInput',
            'value' => '2017 Все права защищены'
        ]
    ],
];
