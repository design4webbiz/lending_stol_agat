<?php

return [
    'name' => 'single_product',
    'title' => Yii::t('text', 'Single Product'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/single_product/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/single_product/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'id' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'hideTitle' => [
            'type' => 'checkbox',
            'value' => 0
        ],
        'product' => [
            'type' => 'select',
            'items' => function () {
                return \yii\helpers\ArrayHelper::map(\app\modules\shop\models\Product::find()->all(), 'id', 'name');
            },
            'value' => ''
        ]
    ],
];
