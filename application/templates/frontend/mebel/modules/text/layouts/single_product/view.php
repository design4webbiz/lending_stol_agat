<?php
/**
 * @var $model \app\modules\text\models\Text;
 */
use yii\helpers\Html;

$product = \app\modules\shop\models\Product::findOne($model->getSetting('product'));
$this->registerJsFile('//yastatic.net/es5-shims/0.0.2/es5-shims.min.js');
$this->registerJsFile('//yastatic.net/share2/share.js');
?>
<?= Html::beginTag('div', ['class' => $model->getSetting('cssClass'), 'id' => $model->getSetting('id')]) ?>
    <?if((int)$model->settings['hideTitle']['value'] !== 1):?>
        <h2><?= $model->title ?></h2>
        <?if($model->subtitle != ''):?>
            <h3><?= $model->subtitle ?></h3>
        <?endif;?>
    <?endif;?>

    <div class="product">
        <h3 class="product__title text-center">
            <?= $product->name ?>
        </h3>

        <div class="product__info">
            <div class="container">
                <div class="col-sm-6">
                    <div class="product__gallery">
                        <div data-uk-slideshow="{}">
                            <div class="uk-slidenav-position">
                                <ul class="uk-slideshow">
                                <?php foreach ($product->getModificationsImages() as $item) {
                                    echo Html::beginTag('li') .
                                        Html::img($item->getUrl('375x280'), ['title' => $item->title, 'alt' => $item->alt]) .
                                        Html::tag('div', $item->title, ['class' => 'image-title']) .
                                        Html::endTag('li');
                                } ?>
                                </ul>
                                <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous uk-hidden-touch" data-uk-slideshow-item="previous"></a>
                                <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next uk-hidden-touch" data-uk-slideshow-item="next"></a>

                                <div class="uk-overlay-panel uk-overlay-bottom">
                                    <ul class="uk-thumbnav uk-flex-center">
                                        <?foreach ($product->getModificationsImages() as $k=>$item): ?>
                                        <li data-uk-slideshow-item="<?= $k ?>">
                                            <a href="javascript:void(0)">
                                                <?= Html::img($item->getUrl('110x80'), ['title' => $item->title, 'alt' => $item->alt]) ?>
                                            </a>
                                        </li>
                                        <?endforeach;?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter" data-counter=""></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="product__fields">
                        <div class="product__fields_materials">
                            <span class="product__fields_materials_ttl">
                                Материалы:
                            </span>
                            <div>
                                <span class="product__fields_ttl">
                                    Столешница:
                                </span>
                                <span class="product__fields_desc">
                                    <?= $product->getField('worktop') ?>
                                </span>
                                    <br>
                                <span class="product__fields_ttl">
                                    Стоевые:
                                </span>
                                <span class="product__fields_desc">
                                    <?= $product->getField('stock') ?>
                                </span>
                                    <br>
                                <span class="product__fields_ttl">
                                    Царга:
                                </span>
                                <span class="product__fields_desc">
                                    <?= $product->getField('tsarga') ?>
                                </span>
                            </div>
                        </div>
                        <div class="product__fields_dimensions">
                            <span class="product__fields_dimensions_ttl">
                                Размеры:
                            </span>
                            <div>
                                <span class="product__fields_ttl">
                                    В*Ш*Г:
                                </span>
                                <span class="product__fields_desc">
                                    <?= $product->getField('dimensions') ?>
                                </span>
                                <br>
                                <span class="product__fields_ttl">
                                    вес:
                                </span>
                                <span class="product__fields_desc">
                                    <?= $product->getField('weight') ?>
                                </span>
                                <br>
                                <span class="product__fields_ttl">
                                    объем:
                                </span>
                                <span class="product__fields_desc">
                                    <?= $product->getField('amount') ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="product__colors">
                        <?= \app\modules\cart\widgets\ChangeOptions::widget([
                            'model'    => $product,
                            'cssClass' => 'colors',
                            'type' => 'radio',
                        ]); ?>
                    </div>

                    <div class="product__count clearfix">
                        <div class="product__count_ttl">
                            <span>
                                кол-во
                            </span>
                        </div>
                        <div class="product__count_desc">
                            <?= \app\modules\cart\widgets\ChangeCount::widget([
                                'model'    => $product,
                                'cssClass' => 'quantity total col-xs-12 mob_center',
                                'downArr'  => '-',
                                'upArr'    => '+',
                            ]); ?>
                        </div>
                    </div>

                    <div class="product__cost">
                        <div class="product__cost_ttl">
                            <span>
                                цена:
                            </span>
                        </div>
                        <div class="product__cost_desc clearfix">
                            <?= \app\modules\shop\widgets\ShowPrice::widget([
                                'model' => $product,
                                'replacePriceID' => 2,
                                'template' => '<p class="old_prise no_padding mob_center">
                                                    <span>[price id="1"]</span>
                                                    <span>руб.</span>
                                                </p>
                                                <p class="total_cost no_padding mob_center">
                                                    [price id="2"]
                                                    <span>руб.</span>
                                                </p>'
                            ]) ?>
                            <div class="product__cost_desc_star">
                                <span>*</span>
                            </div>
                        </div>
                        <div class="product__cost_desc_star-point">
                            <span style="color: #01a8a2;">*</span>
                            <span>
                                +100 руб доставка по Москве
                            </span>
                        </div>
                    </div>
                    <div class="product__buy">
                        <?= \app\modules\cart\widgets\BuyButton::widget([
                            'model'    => $product,
                            'cssClass' => 'buy_button',
                            'text'     => 'заказать',
                            'htmlTag'  => 'button',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $model->text ?>
<?= Html::endTag('div') ?>
