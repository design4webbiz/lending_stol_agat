<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'buy_form',
    'title' => Yii::t('text', 'Buy Form'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/buy_form/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/buy_form/view.php',
    'settings' => [
        'headerForm' => [
            'type' => 'textInput',
            'value' => 'Оформить заказ'
        ],
        'form' => [
            'type' => 'formBuilder',
            'value' => ''
        ]
    ],
];
