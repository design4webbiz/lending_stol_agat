<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'map',
    'title' => Yii::t('text', 'Map'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/map/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/map/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'latCenter' => [
            'type' => 'textInput',
            'value' => '55.74643901'
        ],
        'lngCenter' => [
            'type' => 'textInput',
            'value' => '37.623590'
        ],
        'zoom' => [
            'type' => 'textInput',
            'value' => '15'
        ],
        'lat' => [
            'type' => 'textInput',
            'value' => '55.74409589'
        ],
        'lng' => [
            'type' => 'textInput',
            'value' => '37.61210203'
        ],
        'icon' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'content' => [
            'type' => 'textInput',
            'value' => 'Стол Агат 28.10'
        ],
        'lat2' => [
            'type' => 'textInput',
            'value' => '55.74419251'
        ],
        'lng2' => [
            'type' => 'textInput',
            'value' => '37.63274431'
        ],
        'icon2' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'content2' => [
            'type' => 'textInput',
            'value' => 'Стол Агат 28.10'
        ],
        'headerForm' => [
            'type' => 'textInput',
            'value' => 'ЗАДАТЬ ВОПРОС'
        ],
        'form' => [
            'type' => 'formBuilder',
            'value' => ''
        ],
        'buttonName' => [
            'type' => 'textInput',
            'value' => 'ЗАДАТЬ ВОПРОС'
        ],
        'mobileForm' => [
            'type' => 'textInput',
            'value' => ''
        ]
    ],
];
