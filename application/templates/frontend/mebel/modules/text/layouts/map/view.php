<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use yii\helpers\Html;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>
<?= Html::beginTag('section', [
    'class' => 'map',
    'data' => [
        'coordinate-center-lat' => $model->getSetting('latCenter'),
        'coordinate-center-lng' => $model->getSetting('lngCenter'),
        'coordinate-zoom' => $model->getSetting('zoom'),
        'coordinate-lat' => $model->getSetting('lat'),
        'coordinate-lng' => $model->getSetting('lng'),
        'coordinate-icon' => $model->getSetting('icon'),
        'coordinate-content' => $model->getSetting('content'),
        'coordinate-lat2' => $model->getSetting('lat2'),
        'coordinate-lng2' => $model->getSetting('lng2'),
        'coordinate-icon2' => $model->getSetting('icon2'),
        'coordinate-content2' => $model->getSetting('content2')
    ]
]) ?>

    <div id="map"></div>
    <div class="map__form">
        <div class="map__form_ttl">
            <span class="modal-title"><?= $model->getSetting('headerForm') ?></span>
        </div>
        <div class="map__form_body">
            [form_builder id="<?= $model->getSetting('form') ?>"]
        </div>
    </div>
    <button class="quest-top map__btn-quest col-sm-2 ripplelink" data-toggle="modal" data-target="#<?= $model->getSetting('mobileForm') ?>"><?= $model->getSetting('buttonName') ?></button>
<?= Html::endTag('section') ?>
