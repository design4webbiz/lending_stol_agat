<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'how_we_work',
    'title' => Yii::t('text', 'How We Work'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/how_we_work/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/how_we_work/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'title' => [
            'type' => 'textInput',
            'value' => 'как мы работаем?'
        ],
        'choisImage' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'choisDescription' => [
            'type' => 'textInput',
            'value' => 'выберите товар, нажмите кнопку “заказать” или позвоните нам'
        ],
        'data' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'dataDescription' => [
            'type' => 'textInput',
            'value' => 'оставьте ваши данные'
        ],
        'order' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'orderDescription' => [
            'type' => 'textInput',
            'value' => 'Мы принимаем заказ, согласовываем место и время доставки.'
        ],
        'delivery' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'deliveryDescription' => [
            'type' => 'textInput',
            'value' => 'ждите доставку. оплата поле доставки.'
        ]
    ],
];
