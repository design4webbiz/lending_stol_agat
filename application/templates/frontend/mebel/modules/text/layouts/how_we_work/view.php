<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<section class="how-buy">
    <div class="container">
        <h2 id="how-buy" class="ttl"><?= $model->getSetting('title') ?></h2>
        <div class="how-buy__second">
            <div class="how-buy__item">
                <div class="how-buy__item_img">
                    <img src="<?= $model->settings['choisImage']['value'] ?>">
                </div>
                <div class="how-buy__item_desc">
                    <span>
                        <?= $model->settings['choisDescription']['value'] ?>
                    </span>
                </div>
            </div>
            <div class="how-buy__arrow">
                <img src="images/how-buy-arrow.png" alt="">
            </div>
            <div class="how-buy__item">
                <div class="how-buy__item_img">
                    <img src="<?= $model->settings['data']['value'] ?>">
                </div>
                <div class="how-buy__item_desc">
                    <span>
                        <?= $model->settings['dataDescription']['value'] ?>
                    </span>
                </div>
            </div>
            <div class="how-buy__arrow">
                <img src="images/how-buy-arrow.png" alt="">
            </div>
            <div class="how-buy__item">
                <div class="how-buy__item_img">
                    <img src="<?= $model->settings['order']['value'] ?>">
                </div>
                <div class="how-buy__item_desc">
                    <span>
                        <?= $model->settings['orderDescription']['value'] ?>
                    </span>
                </div>
            </div>
            <div class="how-buy__arrow">
                <img src="images/how-buy-arrow.png" alt="">
            </div>
            <div class="how-buy__item">
                <div class="how-buy__item_img">
                    <img src="<?= $model->settings['delivery']['value'] ?>">
                </div>
                <div class="how-buy__item_desc">
                    <span>
                        <?= $model->settings['deliveryDescription']['value'] ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>
