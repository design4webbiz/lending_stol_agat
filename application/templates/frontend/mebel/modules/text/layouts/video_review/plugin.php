<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'video_review',
    'title' => Yii::t('text', 'Video Review'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/video_review/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/video_review/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'Title' => [
            'type' => 'textInput',
            'value' => 'Видеобзор СТОЛА АГАТ 28.10 серия Лайт'
        ],
        'Video' => [
            'type' => 'textarea',
            'value' => '<iframe width="100%" height="450" src="https://www.youtube.com/embed/LWtYiNXIxxI" frameborder="0" allowfullscreen></iframe>'
        ],
        'subTitle' => [
            'type' => 'textInput',
            'value' => 'Агат 28.10 лайт» –'
        ],
        'Description' => [
            'type' => 'textarea',
            'value' => 'Произведен из ЛДСП высокого качества наивысшего класса экологической безопасности E1.'
        ]
    ],
];
