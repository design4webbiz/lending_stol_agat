<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<section class="video-review">
    <div class="container">
        <h3 class="h3-ttl video-review__ttl"><?= $model->settings['Title']['value'] ?></h3>
        <div class="video-review__video">
            <?= $model->settings['Video']['value'] ?>
        </div>
        <div class="video-review__sub-ttl">
            <?= $model->settings['subTitle']['value'] ?>
        </div>
        <div class="video-review__desc">
            <?= $model->settings['Description']['value'] ?>
        </div>
    </div>
</section>
