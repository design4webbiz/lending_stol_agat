<?php

return [
    'name' => 'products',
    'title' => Yii::t('text', 'Products'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/products/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/products/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'id' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'hideTitle' => [
            'type' => 'checkbox',
            'value' => 0
        ]
    ],
];
