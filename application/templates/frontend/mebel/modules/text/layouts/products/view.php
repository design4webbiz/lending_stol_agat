<?php
/**
 * @var $model \app\modules\text\models\Text;
 */
use yii\helpers\Html;
$categories = \app\modules\shop\models\Category::find()->all();

?>
<?= Html::beginTag('section', ['class' => $model->getSetting('cssClass'), 'id' => $model->getSetting('id')]) ?>
    <div class="container">
        <?php if((int)$model->settings['hideTitle']['value'] !== 1): ?>
            <h2 class="<?= $model->getSetting('cssClass') ?>__ttl ttl"><?= $model->title ?></h2>
            <?php if($model->subtitle != ''): ?>
                <h3><?= $model->subtitle ?></h3>
            <?php endif; ?>
        <?php endif; ?>

        <div class="<?= $model->getSetting('cssClass') ?>__cntnr">
            <?= $model->text ?>
            <?php if(count($categories) > 1): ?>
            <ul class="nav nav-tabs" role="tablist">
                <?php
                    $i = 0;
                    foreach ($categories as $k=>$category):
                ?>
                <li class="<?= $i == 0 ? 'active': '' ?>">
                    <a href="#<?= $category->slug ?>" aria-controls="<?= $category->slug ?>" role="tab" data-toggle="tab">
                        <?= $category->name ?>
                    </a>
                </li>
                <?php
                    $i++;
                    endforeach;
                ?>
            </ul>
            <?php endif; ?>


            <div class="tab-content">
                <?php
                    $i = 0;
                    foreach ($categories as $k=>$category):
                ?>
                    <div class="tab-pane<?= $i == 0 ? ' active': '' ?>" id="<?= $category->slug ?>">
                        <ul class="tab-content__ul scrollbar-light">
                            <?php foreach ($category->products as $product): ?>
                                <?php foreach ($product->modifications as $modification): ?>
                                    <li class="tab-content__li">
                                        <div class="tab-content__li_in">
                                            <div class="<?= $model->getSetting('cssClass') ?>_image">
                                                <?= Html::img($modification->getImage()->getUrl('375x280'), ['title' => $modification->getImage()->title, 'alt' => $modification->getImage()->alt]) ?>
                                            </div>
                                            <div class="<?= $model->getSetting('cssClass') ?>_desc">
                                                <div class="title">
                                                    <?= $product->short_text ?> <span><?= $modification->name ?></span>
                                                </div>
                                            </div>
                                            <div class="<?= $model->getSetting('cssClass') ?>__cost">
                                                <div class="<?= $model->getSetting('cssClass') ?>__cost_desc clearfix">
                                                    <?= \app\modules\shop\widgets\ShowPrice::widget([
                                                        'model' => $product,
                                                        'modificationId' => $modification->id,
                                                        'template' => '<div class="total_cost no_padding mob_center">
                                                                            [price id=1]
                                                                            <div class="total_cost_in">
                                                                                <span>руб.</span>
                                                                                <div class="old_prise no_padding mob_center">
                                                                                    <div class="old_prise_in">
                                                                                        [price id=2]<span>руб.</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>'
                                                    ]) ?>
                                                    <div class="<?= $model->getSetting('cssClass') ?>__cost_desc_star-point">
                                                        <span>
                                                            +100 руб доставка по Москве
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="<?= $model->getSetting('cssClass') ?>__buy">
                                                    <?= \app\modules\cart\widgets\BuyButton::widget([
                                                        'model'    => $product,
                                                        'cssClass' => 'buy_button',
                                                        'text'     => 'заказать',
                                                        'htmlTag'  => 'button',
                                                        'options'  => unserialize($modification->filter_values)
                                                    ]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach;?>
                            <?php endforeach;?>
                        </ul>
                    </div>
                <?php
                    $i++;
                    endforeach;
                ?>
            </div>
        </div>
    </div>
<?= Html::endTag('section') ?>
