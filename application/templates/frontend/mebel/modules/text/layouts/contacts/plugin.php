<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'contacts',
    'title' => Yii::t('text', 'Contacts'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mebel/modules/text/layouts/contacts/preview.png',
    'viewFile' => '@app/templates/frontend/mebel/modules/text/layouts/contacts/view.php',
    'settings' => [
        'cssClass' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'title' => [
            'type' => 'textInput',
            'value' => 'При желании Вы можете посетить наш магазины в красноярске'
        ],
        'background' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'shopName1' => [
            'type' => 'textInput',
            'value' => 'МАГАЗИН 1'
        ],
        'shopAddress1' => [
            'type' => 'textInput',
            'value' => 'Красноярск<br>ул. Красноярск, д.1 строение1, ТЦ Зебра'
        ],
        'shopPhone1' => [
            'type' => 'textInput',
            'value' => '800 123-45-67'
        ],
        'shopMail1' => [
            'type' => 'textInput',
            'value' => 'usikova123@mail.ru'
        ],
        'shopImage1_1' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'shopImage1_2' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'shopImage1_3' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'shopName2' => [
            'type' => 'textInput',
            'value' => 'МАГАЗИН 1'
        ],
        'shopAddress2' => [
            'type' => 'textInput',
            'value' => 'Красноярск<br>ул. Красноярск, д.1 строение1, ТЦ Зебра'
        ],
        'shopPhone2' => [
            'type' => 'textInput',
            'value' => '800 123-45-67'
        ],
        'shopMail2' => [
            'type' => 'textInput',
            'value' => 'usikova123@mail.ru'
        ],
        'shopImage2_1' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'shopImage2_2' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'shopImage2_3' => [
            'type' => 'mediaInput',
            'value' => ''
        ]
    ],
];
