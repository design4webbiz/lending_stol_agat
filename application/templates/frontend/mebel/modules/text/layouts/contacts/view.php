<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>
<section class="contacts" style="background-image: url('<?= $model->getSetting('background') ?>')">
    <div class="container clearfix">
        <h2><?= $model->getSetting('title') ?></h2>
        <div class="contacts__shop scrollbar-light clearfix">
            <div class="contacts__shop_all">
                <div class="contacts__shop_name">
                    <span>
                        <?= $model->getSetting('shopName1') ?>
                    </span>
                </div>
                <div class="contacts__shop_address">
                    <span>
                        <?= $model->getSetting('shopAddress1') ?>
                    </span>
                </div>
                <div class="contacts__shop_phone">
                    <span>
                        тел:&nbsp;
                    </span>
                    <span>
                        <?= $model->getSetting('shopPhone1') ?>
                    </span>
                </div>
                <div class="contacts__shop_mail">
                    <span>
                        e-mail:&nbsp;
                    </span>
                    <span>
                        <?= $model->getSetting('shopMail1') ?>
                    </span>
                </div>
            </div>
            <div class="contacts__shop_img scrollbar-light">
                <div class="contacts__shop_img_in">
                    <img src="<?= $model->getSetting('shopImage1_1') ?>">
                    <img src="<?= $model->getSetting('shopImage1_2') ?>">
                    <img src="<?= $model->getSetting('shopImage1_3') ?>">
                </div>
            </div>
        </div>
        <div class="contacts__shop clearfix">
            <div class="contacts__shop_all">
                <div class="contacts__shop_name">
                    <span>
                        <?= $model->getSetting('shopName2') ?>
                    </span>
                </div>
                <div class="contacts__shop_address">
                    <span>
                        <?= $model->getSetting('shopAddress2') ?>
                    </span>
                </div>
                <div class="contacts__shop_phone">
                    <span>
                        тел:&nbsp;
                    </span>
                    <span>
                        <?= $model->getSetting('shopPhone2') ?>
                    </span>
                </div>
                <div class="contacts__shop_mail">
                    <span>
                        e-mail:&nbsp;
                    </span>
                    <span>
                        <?= $model->getSetting('shopMail2') ?>
                    </span>
                </div>
            </div>
            <div class="contacts__shop_img scrollbar-light">
                <div class="contacts__shop_img_in">
                    <img src="<?= $model->getSetting('shopImage2_1') ?>">
                    <img src="<?= $model->getSetting('shopImage2_2') ?>">
                    <img src="<?= $model->getSetting('shopImage2_3') ?>">
                </div>
            </div>
        </div>
    </div>
</section>
