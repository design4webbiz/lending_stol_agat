<?php

/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 06.04.2016
 * Project: oakcms
 * File name: BaseAsset.php
 */

namespace app\templates\frontend\mebel\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BaseAsset extends AssetBundle
{
    public $sourcePath = '@app/templates/frontend/mebel/web/';
    public $basePath = '@app/templates/frontend/mebel/web/';
    public $publishOptions = ['forceCopy' => true];

    public $css = [
        'css/sweetalert.css',
        'css/style.css',
    ];

    public $js = [
        'js/jquery.scrollbar.min.js',
        '//api-maps.yandex.ru/2.1/?lang=ru_RU',
        'js/sweetalert.min.js',
        'js/script.js',
        'js/y-map.js'

    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD
    ];

    function init()
    {
        parent::init();
    }
}
