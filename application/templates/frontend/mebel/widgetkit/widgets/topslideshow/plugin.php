<?php

return array(

    'name' => 'widget/topslideshow',

    'main' => 'YOOtheme\\Widgetkit\\Widget\\Widget',

    'config' => array(

        'name'  => 'topslideshow',
        'label' => 'Top Slideshow',
        'core'  => false,
        'icon'  => __DIR__.'/widget.svg',
        'view'  => __DIR__.'/views/widget.php',
        'fields' => [
            [
                'type' => 'text',
                'name' => 'name_mebel',
                'label' => 'Title Slider',
            ],
            [
                'type' => 'text',
                'name' => 'sub_name_mebel',
                'label' => 'Sub Title Slider',
            ],
            [
                'type' => 'text',
                'name' => 'action',
                'label' => 'Action',
            ],
            [
                'type' => 'media',
                'name' => 'products',
                'label' => 'Img Products',
            ],

        ],
        'settings' => array(
            'nav'                => 'dotnav',
            'nav_overlay'        => true,
            'nav_align'          => 'center',
            'thumbnail_width'    => '70',
            'thumbnail_height'   => '70',
            'thumbnail_alt'      => false,
            'slidenav'           => 'default',
            'nav_contrast'       => true,
            'animation'          => 'fade',
            'slices'             => '15',
            'duration'           => '500',
            'autoplay'           => false,
            'interval'           => '3000',
            'autoplay_pause'     => true,
            'kenburns'           => false,
            'kenburns_animation' => '',
            'kenburns_duration'  => '15',
            'fullscreen'         => false,
            'min_height'         => '300',

            'media'              => true,
            'image_width'        => 'auto',
            'image_height'       => 'auto',
            'overlay'            => 'none',
            'overlay_animation'  => 'fade',
            'overlay_background' => true,
            'link_media'         => false,

            'title'              => true,
            'content'            => true,
            'title_size'         => 'h3',
            'content_size'       => '',
            'link'               => true,
            'link_style'         => 'button',
            'link_text'          => 'Read more',
            'badge'              => true,
            'badge_style'        => 'badge',

            'link_target'        => false,
            'class'              => ''
        )

    ),

    'events' => array(

        'init.site' => function($event, $app) {
            $app['scripts']->add('uikit2-slideshow', "vendor/assets/uikit/js/components/slideshow.min.js", array('uikit2'));
            $app['scripts']->add('uikit2-slideshow-fx', "vendor/assets/uikit/js/components/slideshow-fx.min.js", array('uikit2'));
        },

        'init.admin' => function($event, $app) {
            $app['angular']->addTemplate('topslideshow.edit', __DIR__.'/views/edit.php', true);
        }

    )

);
